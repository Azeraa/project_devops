# Use an official PHP runtime as a parent image
FROM php:8.3-fpm

# Install Git
RUN apt-get update && \
    apt-get install -y git

# Set Git user name and email
RUN git config --global user.name "Marwen Aydi" && \
    git config --global user.email "aaydi@outlook.fr"

# Install any needed packages specified in requirements.txt
RUN apt-get install -y libpq-dev && \
    docker-php-ext-install pdo pdo_pgsql && \
    curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

# Set the working directory to /app
WORKDIR /app

# Copy only necessary files for composer install
COPY composer.json composer.lock symfony.lock ./

# Install Composer dependencies
RUN composer install --no-scripts --no-autoloader

# Copy the rest of the application
COPY . .
# Install Composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

# Run composer dump-autoload to optimize autoloader
RUN composer dump-autoload --optimize

# Make port 9000 available to the world outside this container
EXPOSE 9000

# Define environment variable
ENV SYMFONY_ENV=dev

# Run app.py when the container launches
CMD ["php", "bin/console", "server:run", "0.0.0.0:9000"]
